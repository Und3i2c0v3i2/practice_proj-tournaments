package practice.amateurleaguestournaments.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import practice.amateurleaguestournaments.model.TournamentType;

@Repository
public interface TournamentTypeRepository 
	extends JpaRepository<TournamentType, Long>{

}
