var mainApp = angular.module("mainApp", ["ngRoute"]);

mainApp.config(['$routeProvider', function($routeProvider) {

	$routeProvider
		.when('/', {
			templateUrl : '/app/html/home.html',
			controller: 'homeController'
		})
		.when('/contestants/edit/:cid', {
			templateUrl : '/app/html/edit.html'
		})
		.when('/tournaments/result/:tid', {
			templateUrl : '/app/html/add-result.html'
		})
		.otherwise({
			redirectTo: '/'
		});
}]);


mainApp.controller("homeController", function($scope, $location, $http, $routeParams){
  
  $scope.message = "Amateur Leagues Tournaments";

	var baseTournamentsUrl = "/api/tournaments";
	var baseContestantsUrl = "/api/contestants";

	$scope.page = 0;
	$scope.totalPages = 0;

	$scope.tournaments = [];
	$scope.contestants =[];
	
	$scope.saved = {};
	
	$scope.searchParams = {};




//	---------------------------------------------------------------------------------
// 			
//	---------------------------------------------------------------------------------

	var getTournaments = function(){
		$http.get(baseTournamentsUrl).then(
			function(response){
				$scope.tournaments = response.data;
			},
			function(){
				alert("Couldn't get tournaments");
			}
		);
	}

	getTournaments();


//   ------------------------------------------------------------------------------------
//     
//   ------------------------------------------------------------------------------------



	$scope.getContestants = function(){
		var config = {params: {}};
		$scope.page = 0;
		config.params.page = $scope.page;
		
		var contestantsUrl = baseTournamentsUrl + "/" + $scope.tournamentId + "/contestants";
		$http.get(contestantsUrl, config).then(
			function(response){
				$scope.contestants = response.data;
				$scope.totalPages = response.headers("totalPages");
			},
			function(){
				alert("Couldn't get contestants");
			}
		);
	}
	
	$scope.getContestantsSearch = function(){
		var config = {params: {}};
		config.params.page = $scope.page;
		
		// required param for search - tournament Id
		config.params.tournamentId = $scope.tournamentId;
		
		// optional param for search - contestant name
		if($scope.searchParams.name != ""){
			config.params.name = $scope.searchParams.name;
		}
		
		$http.get(baseContestantsUrl, config).then(
			function(response){
				$scope.contestants = response.data;
				$scope.totalPages = response.headers("totalPages");
			},
			function(){
				alert("Couldn't get contestants");
			}
		);
	}
				

	$scope.remove = function(id){
		var deleteUrl = baseContestantsUrl + "/" + id;

		$http.delete(deleteUrl).then(
			function(){
				$scope.getContestantsSearch();
				getTournaments();
			},
			function(){
				alert("Couldn't delete contestant");
			}
		);
	}


	$scope.save = function(){
		$http.post(baseContestantsUrl, $scope.saved)
			.then(
				function success(response){
					$scope.saved = {};
					$scope.addForm.$setPristine();
					$scope.addForm.$setUntouched();
//					$scope.getContestants();
//					$scope.getContestantsSearch();
					getTournaments();
				},
				function error(){
					alert("Couldn't save contestant");
				}
			);
	}



	$scope.goToEdit = function(id){
		$location.path("/contestants/edit/" + id);
	}
	
	$scope.goToAddResult = function(){
		$location.path("/tournaments/result/" + $scope.tournamentId);
	}


// 	------------------------------------
// 		SEARCH AND PAGINATION 
// 	------------------------------------

	$scope.doSearch = function(id){
		$scope.page = 0;
		$scope.getContestantsSearch();
	}

	$scope.changePage = function(direction){
		$scope.page = $scope.page + direction;
		$scope.getContestantsSearch();
	}

});
