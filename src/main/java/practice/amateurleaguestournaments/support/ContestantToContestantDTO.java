package practice.amateurleaguestournaments.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import practice.amateurleaguestournaments.model.Contestant;
import practice.amateurleaguestournaments.web.dto.ContestantDTO;

@Component
public class ContestantToContestantDTO 
	implements Converter<Contestant, ContestantDTO>{

	@Override
	public ContestantDTO convert(Contestant c) {
		ContestantDTO retVal = new ContestantDTO();
		retVal.setId(c.getId());
		retVal.setName(c.getName());
		retVal.setPlace(c.getPlace());
		retVal.setEmail(c.getEmail());
		retVal.setPlayed(c.getPlayed());
		retVal.setPoints(c.getPoints());
		retVal.setTournamentName(c.getTournament().getName());
		retVal.setTournamentId(c.getTournament().getId());
		
		return retVal;
	}

	public List<ContestantDTO> convert(List<Contestant> contestants) {
		List<ContestantDTO> retVal = new ArrayList<>();
		for(Contestant c : contestants) {
			retVal.add(convert(c));
		}
		return retVal;
	}
}
