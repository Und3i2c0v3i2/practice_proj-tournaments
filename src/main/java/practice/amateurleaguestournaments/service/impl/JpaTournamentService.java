package practice.amateurleaguestournaments.service.impl;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import practice.amateurleaguestournaments.model.Tournament;
import practice.amateurleaguestournaments.model.Contestant;
import practice.amateurleaguestournaments.repository.TournamentRepository;
import practice.amateurleaguestournaments.repository.ContestantRepository;
import practice.amateurleaguestournaments.service.TournamentService;

@Service
public class JpaTournamentService 
	implements TournamentService {

	@Autowired
	private TournamentRepository tournamentRepository;
	@Autowired
	private ContestantRepository contestantRepository;
	
	@Override
	public List<Tournament> findAll() {
		return tournamentRepository.findAll();
	}

	@Override
	public Tournament findOne(Long id) {
		return tournamentRepository.findById(id)
								   .orElseThrow(() -> new EntityNotFoundException("Couldn't find tournament with id " + id));
	}

	@Override
	public Tournament save(Tournament t) {
		return tournamentRepository.save(t);
	}

	@Override
	public Tournament result(Long tournamentId, Long contestantId1, Long contestantId2, Long matchResult) {
		
		Tournament tournament = findOne(tournamentId);
		int w = tournament.getTournamentType().getWinPoints();
		int l = tournament.getTournamentType().getLossPoints();
		int t = tournament.getTournamentType().getTiePoints();
		
		Contestant contestant1 = contestantRepository.findById(contestantId1)
													 .orElseThrow(() -> new EntityNotFoundException("Couldn't find contestant with id " + contestantId1));
		Contestant contestant2 = contestantRepository.findById(contestantId2)
													 .orElseThrow(() -> new EntityNotFoundException("Couldn't find contestant with id " + contestantId2));
		
		contestant1.setPlayed(contestant1.getPlayed() + 1);
		contestant2.setPlayed(contestant2.getPlayed() + 1);
		
		tournament.addContestant(contestant1);
		tournament.addContestant(contestant2);
		
		// contestant1 won
		if(contestant1.getId() == matchResult) {
			contestant1.setPoints(contestant1.getPoints() + w);
			contestant2.setPoints(contestant2.getPoints() + l);
		} 
		// contestant 2 won
		else if(contestant2.getId() == matchResult) {
			contestant2.setPoints(contestant2.getPoints() + w);
			contestant1.setPoints(contestant1.getPoints() + l);
		} 
		// tie
		else {
			contestant1.setPoints(contestant1.getPoints() + t);
			contestant2.setPoints(contestant2.getPoints() + t);
		}
		
		
		contestantRepository.save(contestant1);
		contestantRepository.save(contestant2);
		tournamentRepository.save(tournament);
		
		
		return tournament;
	}

}
