package practice.amateurleaguestournaments.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import practice.amateurleaguestournaments.model.Tournament;
import practice.amateurleaguestournaments.model.Contestant;
import practice.amateurleaguestournaments.service.TournamentService;
import practice.amateurleaguestournaments.service.ContestantService;
import practice.amateurleaguestournaments.support.TournamentToTournamentDTO;
import practice.amateurleaguestournaments.support.ContestantToContestantDTO;
import practice.amateurleaguestournaments.web.dto.TournamentDTO;
import practice.amateurleaguestournaments.web.dto.ContestantDTO;

@Controller
@RequestMapping(value="api/tournaments")
public class ApiTournamentController {

	@Autowired
	private TournamentService tournamentService;
	@Autowired
	private TournamentToTournamentDTO toDto;
	@Autowired
	private ContestantService contestantService;
	@Autowired
	private ContestantToContestantDTO contestantDto;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<TournamentDTO>> getAll() {
		List<Tournament> tournaments = tournamentService.findAll();
		
		return new ResponseEntity<>(toDto.convert(tournaments), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public ResponseEntity<TournamentDTO> getOne(@PathVariable Long id) {
		
		Tournament t = tournamentService.findOne(id);
		if(t == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(toDto.convert(t), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}/contestants")
	public ResponseEntity<List<ContestantDTO>> getUcesniciTakmicenja(
			@PathVariable Long id,
			@RequestParam(defaultValue="0") int page) {
		
		Page<Contestant> ucesnici = contestantService.findByTournamentId(page, id);

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(ucesnici.getTotalPages()) );
		
		return new ResponseEntity<>(contestantDto.convert(ucesnici.getContent()), headers,	HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}/result")
	public ResponseEntity<TournamentDTO> rezultat(
			@PathVariable Long id,
			@RequestParam(value="contestantId1") Long contestantId1,
			@RequestParam(value="contestantId2") Long contestantId2,
			@RequestParam(value="matchResult") Long result) {
		
		Tournament t = tournamentService.result(id, contestantId1, contestantId2, result);
		
		return new ResponseEntity<>(toDto.convert(t), HttpStatus.OK);
	}
}
