package practice.amateurleaguestournaments.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import practice.amateurleaguestournaments.model.Tournament;
import practice.amateurleaguestournaments.model.Contestant;
import practice.amateurleaguestournaments.service.TournamentService;
import practice.amateurleaguestournaments.service.ContestantService;
import practice.amateurleaguestournaments.web.dto.ContestantDTO;


@Component
public class ContestantDTOToContestant 
	implements Converter<ContestantDTO, Contestant> {

	@Autowired
	private ContestantService contestantService;
	@Autowired 
	private TournamentService tournamentService;
	
	@Override
	public Contestant convert(ContestantDTO dto) {
		Contestant contestant;
		
		if(dto.getId() == null) {
			contestant = new Contestant();
		} else {
			contestant = contestantService.findOne(dto.getId());
		}
		
		Tournament t = tournamentService.findOne(dto.getTournamentId());
		
		if(t != null) {
			contestant.setTournament(t);
		}
		
		contestant.setName(dto.getName());
		contestant.setPlace(dto.getPlace());
		contestant.setEmail(dto.getEmail());
		contestant.setPlayed(dto.getPlayed());
		contestant.setPoints(dto.getPoints());
		
		return contestant;
	}
	
	public List<Contestant> convert(List<ContestantDTO> dtos) {
		List<Contestant> retVal = new ArrayList<>();
		for(ContestantDTO dto : dtos) {
			retVal.add(convert(dto));
		}
		return retVal;
	}

}
