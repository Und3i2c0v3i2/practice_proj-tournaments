package practice.amateurleaguestournaments.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import practice.amateurleaguestournaments.model.Tournament;

@Repository
public interface TournamentRepository 
	extends JpaRepository<Tournament, Long> {

}
