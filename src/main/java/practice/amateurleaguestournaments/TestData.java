package practice.amateurleaguestournaments;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import practice.amateurleaguestournaments.model.TournamentType;
import practice.amateurleaguestournaments.model.Tournament;
import practice.amateurleaguestournaments.model.Contestant;
import practice.amateurleaguestournaments.service.TournamentTypeService;
import practice.amateurleaguestournaments.service.TournamentService;
import practice.amateurleaguestournaments.service.ContestantService;

@Component
public class TestData {

	@Autowired
	private TournamentService tournamentService;
	@Autowired
	private ContestantService contestantService;
	@Autowired
	private TournamentTypeService tournamentTypeService;
	
	
	@PostConstruct
	public void init() {
		
		TournamentType tt1 = new TournamentType();
		tt1.setName("Type 1");
		tt1.setContestantsNo(20);
		tt1.setWinPoints(10);
		tt1.setTiePoints(5);
		tt1.setLossPoints(2);
		
		TournamentType tt2 = new TournamentType();
		tt2.setName("Type 2");
		tt2.setContestantsNo(15);
		tt2.setWinPoints(8);
		tt2.setTiePoints(3);
		tt2.setLossPoints(1);
		
		TournamentType tt3 = new TournamentType();
		tt3.setName("Type 3");
		tt3.setContestantsNo(8);
		tt3.setWinPoints(5);
		tt3.setTiePoints(2);
		tt3.setLossPoints(0);
		
		tournamentTypeService.save(tt1);
		tournamentTypeService.save(tt2);
		tournamentTypeService.save(tt3);
		
		Tournament t1 = new Tournament();
		t1.setName("Super League");
		t1.setTournamentType(tt1);
		Tournament t2 = new Tournament();
		t2.setName("High League");
		t2.setTournamentType(tt2);
		Tournament t3 = new Tournament();
		t3.setName("Small League");
		t3.setTournamentType(tt3);
		
		tournamentService.save(t1);
		tournamentService.save(t2);
		tournamentService.save(t3);
		
		Contestant c1 = new Contestant();
		c1.setName("A");
		c1.setEmail("a@example.com");
		c1.setTournament(t1);
		c1.setPlace("Neverlend");
		
		Contestant c2 = new Contestant();
		c2.setName("B");
		c2.setEmail("b@example.com");
		c2.setTournament(t1);
		c2.setPlace("Neverland");
		
		Contestant c3 = new Contestant();
		c3.setName("C");
		c3.setEmail("c@example.com");
		c3.setTournament(t2);
		c3.setPlace("Middle Earth");
		
		Contestant c4 = new Contestant();
		c4.setName("D");
		c4.setEmail("d@example.com");
		c4.setTournament(t3);
		c4.setPlace("Middle Earth");
		
		Contestant c5 = new Contestant();
		c5.setName("E");
		c5.setEmail("e@example.com");
		c5.setTournament(t2);
		c5.setPlace("Middle Earth");
		
		Contestant c6 = new Contestant();
		c6.setName("F");
		c6.setEmail("f@example.com");
		c6.setTournament(t1);
		c6.setPlace("Oz");
		
		Contestant c7 = new Contestant();
		c7.setName("G");
		c7.setEmail("g@example.com");
		c7.setTournament(t1);
		c7.setPlace("Oz");
		
		Contestant c8 = new Contestant();
		c8.setName("H");
		c8.setEmail("h@example.com");
		c8.setTournament(t1);
		c8.setPlace("Erewhon");
		
		Contestant c9 = new Contestant();
		c9.setName("I");
		c9.setEmail("i@example.com");
		c9.setTournament(t1);
		c9.setPlace("Erewhon");
		
		Contestant c10 = new Contestant();
		c10.setName("J");
		c10.setEmail("j@example.com");
		c10.setTournament(t1);
		c10.setPlace("Erewhon");
		
		Contestant c11 = new Contestant();
		c11.setName("K");
		c11.setEmail("k@example.com");
		c11.setTournament(t1);
		c11.setPlace("Shangri-La");
		
		
		contestantService.save(c1);
		contestantService.save(c2);
		contestantService.save(c3);
		contestantService.save(c4);
		contestantService.save(c5);
		contestantService.save(c6);
		contestantService.save(c7);
		contestantService.save(c8);
		contestantService.save(c9);
		contestantService.save(c10);
		contestantService.save(c11);
	}
}
