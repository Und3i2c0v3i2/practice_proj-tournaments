package practice.amateurleaguestournaments.service;

import java.util.List;

import practice.amateurleaguestournaments.model.Tournament;

public interface TournamentService {

	public List<Tournament> findAll();
	public Tournament findOne(Long id);
	public Tournament save(Tournament t);
	
	public Tournament result(Long tournamentId, Long contestantId1, Long contestantId2, Long matchResult);
}
