package practice.amateurleaguestournaments.web.dto;


public class TournamentDTO {

	private Long id;
	private String name;
	private int maxContestants;
	private int noContestants;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String naziv) {
		this.name = naziv;
	}
	public int getMaxContestants() {
		return maxContestants;
	}
	public void setMaxContestants(int maxContestants) {
		this.maxContestants = maxContestants;
	}
	public int getNoContestants() {
		return noContestants;
	}
	public void setNoContestants(int noContestants) {
		this.noContestants = noContestants;
	}
	
}
