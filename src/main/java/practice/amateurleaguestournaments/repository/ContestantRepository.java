package practice.amateurleaguestournaments.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import practice.amateurleaguestournaments.model.Contestant;

@Repository
public interface ContestantRepository 
	extends JpaRepository<Contestant, Long>{

	Page<Contestant> findByTournamentId(Long id, Pageable page);

	@Query("SELECT c FROM Contestant c WHERE "
			+ "(:name IS NULL or c.name like :name ) AND "
			+ "(c.tournament.id = :tournamentId) "
			)
	public Page<Contestant> search(
			@Param("name") String name, 
			@Param("tournamentId") Long tournamentId, 
			Pageable pageRequest);


}
