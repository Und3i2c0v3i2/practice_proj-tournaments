mainApp.controller("addResultCtrl", function($scope, $location, $http, $routeParams){

	$scope.contestants = [];
	
	$scope.contestant1 = "";
	$scope.contestant2 = "";
	$scope.result = "";


	var getContestants = function(){
		var contestantsUrl = "/api/tournaments/" + $routeParams.tid +"/contestants";
			$http.get(contestantsUrl).then(
				function(response){
					$scope.contestants = response.data;
				},
				function(){
					alert("Couldn't get contestants");
				}
			);
	}

	getContestants();
	

	$scope.save = function(){		
		var config = {params: {}};
		config.params.contestantId1 = $scope.contestantId1;
		config.params.contestantId2 = $scope.contestantId2;
		config.params.matchResult = $scope.result;
		
		var addResult = "/api/tournaments/" + $routeParams.tid + "/result";
		
		$http.get(addResult, config).then(
			function(response){
				$location.path("/");
			},
			function(){
				alert("Couldn't save result");
			}
		);
	}
	
	

});
