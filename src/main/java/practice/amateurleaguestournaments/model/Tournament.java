package practice.amateurleaguestournaments.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Tournament {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	@ManyToOne
	private TournamentType tournamentType;
	@OneToMany(mappedBy="tournament", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<Contestant> contestants = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TournamentType getTournamentType() {
		return tournamentType;
	}
	public void setTournamentType(TournamentType tournamentType) {
		this.tournamentType = tournamentType;
		if(tournamentType != null && !tournamentType.getTournaments().contains(this)){
			tournamentType.getTournaments().add(this);
		}
	}
	public List<Contestant> getContestants() {
		return contestants;
	}
	public void setContestants(List<Contestant> contestants) {
		this.contestants = contestants;
	}
	
	public void addContestant(Contestant contestant){
		this.contestants.add(contestant);
		if(!this.equals(contestant.getTournament())){
			contestant.setTournament(this);
		}
	}
	
}
