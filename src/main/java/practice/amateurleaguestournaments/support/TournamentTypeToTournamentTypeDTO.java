package practice.amateurleaguestournaments.support;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import practice.amateurleaguestournaments.model.TournamentType;
import practice.amateurleaguestournaments.web.dto.TournamentTypeDTO;

@Component
public class TournamentTypeToTournamentTypeDTO 
	implements Converter<TournamentType, TournamentTypeDTO>{

	@Override
	public TournamentTypeDTO convert(TournamentType source) {

		TournamentTypeDTO dto = new TournamentTypeDTO();
		dto.setId(source.getId());
		return dto;
	}

}
