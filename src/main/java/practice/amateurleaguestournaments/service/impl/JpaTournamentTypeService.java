package practice.amateurleaguestournaments.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import practice.amateurleaguestournaments.model.TournamentType;
import practice.amateurleaguestournaments.repository.TournamentTypeRepository;
import practice.amateurleaguestournaments.service.TournamentTypeService;

@Service
public class JpaTournamentTypeService
	implements TournamentTypeService {

	@Autowired
	private TournamentTypeRepository tournamentTypeRepository;
	
	@Override
	public TournamentType save(TournamentType t) {
		return tournamentTypeRepository.save(t);
	}

}
