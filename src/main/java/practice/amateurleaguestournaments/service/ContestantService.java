package practice.amateurleaguestournaments.service;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.query.Param;

import practice.amateurleaguestournaments.model.Contestant;

public interface ContestantService {

	public Page<Contestant> findAll(int page);	
	public Contestant findOne(Long id);
	public Contestant save(Contestant contestant);
	public void remove(Long id);
	public Page<Contestant> search(
			@Param("name") String name, 
			@Param("tournamentId") Long tournamentId,  
			int page);


	public Page<Contestant> findByTournamentId(int page, Long id);
	
	
}
