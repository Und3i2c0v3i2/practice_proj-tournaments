package practice.amateurleaguestournaments.service;

import practice.amateurleaguestournaments.model.TournamentType;

public interface TournamentTypeService {

	public TournamentType save(TournamentType tournamentType);
}
