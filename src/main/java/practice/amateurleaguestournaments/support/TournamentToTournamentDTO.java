package practice.amateurleaguestournaments.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import practice.amateurleaguestournaments.model.Tournament;
import practice.amateurleaguestournaments.web.dto.TournamentDTO;

@Component
public class TournamentToTournamentDTO 
	implements Converter<Tournament, TournamentDTO>{

	@Override
	public TournamentDTO convert(Tournament tournament) {
		TournamentDTO retVal = new TournamentDTO();
		retVal.setId(tournament.getId());
		retVal.setName(tournament.getName());
		retVal.setNoContestants(tournament.getContestants().size());
		retVal.setMaxContestants(tournament.getTournamentType().getContestantsNo());;
		
		return retVal;
	}

	public List<TournamentDTO> convert(List<Tournament> tournaments) {
		List<TournamentDTO> retVal = new ArrayList<>();
		for(Tournament t : tournaments) {
			retVal.add(convert(t));
		}
		return retVal;	
	}
}
