package practice.amateurleaguestournaments.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class TournamentType {

	@Id
	@GeneratedValue
	private Long id;
	private String name;
	private int contestantsNo;
	private int winPoints; 
	private int tiePoints; 
	private int lossPoints; 
	@OneToMany(mappedBy="tournamentType", fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private List<Tournament> tournaments = new ArrayList<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getContestantsNo() {
		return contestantsNo;
	}
	public void setContestantsNo(int contestantsNo) {
		this.contestantsNo = contestantsNo;
	}
	public int getWinPoints() {
		return winPoints;
	}
	public void setWinPoints(int winPoints) {
		this.winPoints = winPoints;
	}
	public int getTiePoints() {
		return tiePoints;
	}
	public void setTiePoints(int tiePoints) {
		this.tiePoints = tiePoints;
	}
	public int getLossPoints() {
		return lossPoints;
	}
	public void setLossPoints(int lossPoints) {
		this.lossPoints = lossPoints;
	}
	public List<Tournament> getTournaments() {
		return tournaments;
	}
	public void setTournaments(List<Tournament> tournaments) {
		this.tournaments = tournaments;
	}
	public void addTournament(Tournament t) {
		this.tournaments.add(t);
		if(!this.equals(t.getTournamentType())){
			t.setTournamentType(this);
		}
	}
	
	
}
