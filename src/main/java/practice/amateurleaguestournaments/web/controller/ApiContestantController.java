package practice.amateurleaguestournaments.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import practice.amateurleaguestournaments.model.Contestant;
import practice.amateurleaguestournaments.model.Tournament;
import practice.amateurleaguestournaments.service.ContestantService;
import practice.amateurleaguestournaments.service.TournamentService;
import practice.amateurleaguestournaments.support.ContestantDTOToContestant;
import practice.amateurleaguestournaments.support.ContestantToContestantDTO;
import practice.amateurleaguestournaments.web.dto.ContestantDTO;

@Controller
@RequestMapping(value="api/contestants")
public class ApiContestantController {
	
	@Autowired
	private ContestantService contestantService;
	@Autowired
	private TournamentService tournamentService;
	@Autowired
	private ContestantDTOToContestant toContestant;
	@Autowired
	private ContestantToContestantDTO toDto;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ContestantDTO>> getAll(
			@RequestParam(required=false) String name,
			@RequestParam(required=true) Long tournamentId,
			@RequestParam(value="page", defaultValue="0") int page){
		
		Page<Contestant> contestantPage = null;
		
		if(name != null) {
			contestantPage = contestantService.search(name, tournamentId, page);
		}
		else {
			contestantPage = contestantService.findByTournamentId(page, tournamentId);
		}

		HttpHeaders headers = new HttpHeaders();
		headers.add("totalPages", Integer.toString(contestantPage.getTotalPages()) );
		
		return new ResponseEntity<>(
				toDto.convert(contestantPage.getContent()), headers, HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	ResponseEntity<ContestantDTO> getOne(@PathVariable Long id){
		Contestant contestant = contestantService.findOne(id);
		if(contestant == null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<>(toDto.convert(contestant), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	ResponseEntity<Void> delete(@PathVariable Long id){
		
		Contestant deleted = contestantService.findOne(id);
		
		if(deleted == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			contestantService.remove(id);
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
		}
		
	}
	
	@RequestMapping(method=RequestMethod.POST,
					consumes="application/json")
	public ResponseEntity<ContestantDTO> add(
			@Validated @RequestBody ContestantDTO newContestantDTO){
		
		Contestant saved = contestantService.save(toContestant.convert(newContestantDTO));
		
		return new ResponseEntity<>(toDto.convert(saved), HttpStatus.CREATED);
	}
	
	
	@RequestMapping(method=RequestMethod.PUT,
			value="/{id}",
			consumes="application/json")
	public ResponseEntity<ContestantDTO> edit(
			@Validated @RequestBody ContestantDTO contestantDTO,
			@PathVariable Long id){
		
		if(!id.equals(contestantDTO.getId())){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		Contestant persisted = contestantService.save(toContestant.convert(contestantDTO));
		
		return new ResponseEntity<>(toDto.convert(persisted), HttpStatus.OK);
	}
	
	
	@ExceptionHandler(value=DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
}

}
