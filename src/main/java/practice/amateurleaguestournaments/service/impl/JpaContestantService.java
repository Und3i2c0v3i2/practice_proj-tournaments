package practice.amateurleaguestournaments.service.impl;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import practice.amateurleaguestournaments.model.Contestant;
import practice.amateurleaguestournaments.repository.ContestantRepository;
import practice.amateurleaguestournaments.service.ContestantService;

@Service
public class JpaContestantService 
	implements ContestantService {

	@Autowired
	private ContestantRepository contestantRepository;
	
	
	@Override
	public Page<Contestant> findAll(int page) {
		return contestantRepository.findAll(PageRequest.of(page, 5));
	}

	@Override
	public Contestant findOne(Long id) {
		return contestantRepository.findById(id)
								   .orElseThrow(() -> new EntityNotFoundException("Couldn't find contestant with id " + id));
	}

	@Override
	public Contestant save(Contestant contestant) {
		return contestantRepository.save(contestant);
	}


	@Override
	public void remove(Long id) {
		contestantRepository.deleteById(id);
	}

	@Override
	public Page<Contestant> findByTournamentId(int page, Long id) {
		return contestantRepository.findByTournamentId(id, PageRequest.of(page, 5));
	}

	@Override
	public Page<Contestant> search(String name, Long tournamentId, int page) {
		
		if(name != null) {
			name = '%' + name + '%';
		}
		return contestantRepository.search(name, tournamentId, PageRequest.of(page, 5));
	}
	
}

