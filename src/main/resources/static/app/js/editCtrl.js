mainApp.controller("editController", function($scope, $location, $http, $routeParams){

	var urlEdit = "/api/contestants/" + $routeParams.cid;
	$scope.edited = {};
	$scope.tournaments = [];

	var getContestantToEdit = function(){
		$http.get(urlEdit).then(
			function success(response){
				$scope.edited = response.data;
			},
			function error(){
				alert("Couldn't fetch contestant for editing");
			}
		);
	}

	var getTournaments = function(){
		$http.get("/api/tournaments").then(
			function(response){
				$scope.tournaments = response.data;
				getContestantToEdit();
			},
			function(){
				alert("Couldn't fetch tournaments");
			}
		);
	}
	
	getTournaments();
	

	$scope.save = function(){
		$http.put(urlEdit, $scope.edited).then(
			function success(){
				$location.path("/");
			},
			function error(){
				alert("Couldn't edit contestant");
			}
		);
	}

});
