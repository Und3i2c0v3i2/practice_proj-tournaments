package practice.amateurleaguestournaments.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Email;

import org.hibernate.validator.constraints.Length;

@Entity
public class Contestant {

	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column(nullable=false, unique=true)
	@Length(min=1, max=40)
	private String name;
	private String place;
	@Column(nullable=false)
	@Email
	private String email;
	private int played; // number of played matches, initially 0
	private int points; // number of points, initally 0
	@ManyToOne
	private Tournament tournament;
	
	
	public Contestant() {
		super();
		this.played = 0;
		this.points = 0;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPlayed() {
		return played;
	}
	public void setPlayed(int played) {
		this.played = played;
	}
	public Tournament getTournament() {
		return tournament;
	}
	public void setTournament(Tournament tournament) {
		this.tournament = tournament;
		if(tournament!=null && !tournament.getContestants().contains(this)){
			tournament.getContestants().add(this);
		}
	}
	
}
